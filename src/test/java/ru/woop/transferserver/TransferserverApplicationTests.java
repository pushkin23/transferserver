package ru.woop.transferserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.woop.transferserver.controller.Controller;
import ru.woop.transferserver.exceptions.InsufficientFundsException;
import ru.woop.transferserver.model.DbAnalogue;
import ru.woop.transferserver.model.entities.Client;
import ru.woop.transferserver.model.entities.Transfer;

import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import ru.woop.transferserver.model.entities.TransferRecord;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class TransferserverApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private DbAnalogue dbServer;

	@Autowired
	private WebApplicationContext wac;

	@Ignore
	private <T> String getObjAsString(T obj) throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(obj);
	}

	@BeforeEach
	public void init() throws InsufficientFundsException {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		Client client = new Client();
		client.addAccount("money", 8800.5);
		client.addAccount("cash", 5535.35);

		Transfer transfer = new Transfer("money", "cash", 555.5);
		Date initialDate = new Date();
		long uniqueIdToCheck = 222;

		client.addTransfer(transfer, uniqueIdToCheck, initialDate);
		dbServer.addClient("Vasily", client);
	}

	@Test
	void historyCreatedTest() throws Exception {
		Transfer transfer = new Transfer("money", "cash", 555.5);
		Date initialDate = new Date();
		long uniqueIdToCheck = 222;
		TransferRecord record = new TransferRecord(uniqueIdToCheck, transfer, initialDate);

		List<TransferRecord> transferList = Collections.singletonList(record);
		this.mockMvc.perform(get("/order/Vasily/history"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string(getObjAsString(transferList)));
	}

	@Test
	void validTransferTest() throws Exception {
		Transfer newTransfer = new Transfer("money", "cash", 1111.1);
		this.mockMvc.perform(post("/order/Vasily/transfer")
					.contentType(MediaType.APPLICATION_JSON)
					.content(getObjAsString(newTransfer)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	void validSenderAccountAmount() throws Exception {
		Transfer newTransfer = new Transfer("money", "cash", 1111.1);
		this.mockMvc.perform(post("/order/Vasily/transfer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(getObjAsString(newTransfer)))
				.andDo(print());
		Client client = dbServer.getClientMap().get("Vasily");
		Assert.assertEquals(7133.9, client.getAccounts().get("money"), 0.5d);
	}

	@AfterEach
	void clear(){
		dbServer.deleteDb();
	}

}
