package ru.woop.transferserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.woop.transferserver.exceptions.InsufficientFundsException;
import ru.woop.transferserver.model.DbAnalogue;
import ru.woop.transferserver.model.entities.Client;
import ru.woop.transferserver.model.entities.Transfer;
import ru.woop.transferserver.model.entities.TransferRecord;
import org.junit.runners.Suite.SuiteClasses;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@SuiteClasses({TransferserverApplicationTests.class})
public class WrongCasesTests {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DbAnalogue dbServer;

    @Autowired
    private WebApplicationContext wac;

    @Ignore
    private <T> String getObjAsString(T obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }

    @BeforeEach
    public void init() throws InsufficientFundsException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        Client client = new Client();
        client.addAccount("money", 8800.5);
        client.addAccount("cash", 5535.35);

        Transfer transfer = new Transfer("money", "cash", 555.5);
        Date initialDate = new Date();
        long uniqueIdToCheck = 222;

        client.addTransfer(transfer, uniqueIdToCheck, initialDate);
        dbServer.addClient("Vasily", client);
    }

    @Test
    void accessWrongTransfer() throws Exception {
        this.mockMvc.perform(get("/order/Vasily/123"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void accessWrongClientWithGettingTransferId() throws Exception {
        this.mockMvc.perform(get("/order/Grigory/222"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void accessWrongClientTest() throws Exception {
        this.mockMvc.perform(get("/order/Grigory/history"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void validProcessingForLargeTransfer() throws Exception{
        Transfer largeTransfer = new Transfer("money", "cash", 8888.8);
        this.mockMvc.perform(post("/order/Vasily/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjAsString(largeTransfer)))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void transferFromWrongAccount() throws Exception{
        Transfer wrongTransfer = new Transfer("maneyss", "cash", 8888.8);
        this.mockMvc.perform(post("/order/Vasily/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjAsString(wrongTransfer)))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void transferToWrongAccount() throws Exception{
        Transfer largeTransfer = new Transfer("money", "catch", 8888.8);
        this.mockMvc.perform(post("/order/Vasily/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(getObjAsString(largeTransfer)))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @AfterEach
    void clear(){
        dbServer.deleteDb();
    }
}
