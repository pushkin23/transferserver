package ru.woop.transferserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransferserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferserverApplication.class, args);
	}

}
