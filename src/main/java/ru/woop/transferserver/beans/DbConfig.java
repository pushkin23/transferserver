package ru.woop.transferserver.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.woop.transferserver.model.DbAnalogue;

@Configuration
public class DbConfig {
    @Bean
    public DbAnalogue getDbAnalogue(){
        return new DbAnalogue();
    }
}
