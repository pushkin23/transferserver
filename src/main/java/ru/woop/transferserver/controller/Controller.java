package ru.woop.transferserver.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.woop.transferserver.exceptions.InsufficientFundsException;
import ru.woop.transferserver.model.DbAnalogue;
import ru.woop.transferserver.model.Facade;
import ru.woop.transferserver.model.entities.Client;
import ru.woop.transferserver.model.entities.Transfer;
import ru.woop.transferserver.model.entities.TransferRecord;

import java.util.ArrayList;
import java.util.NoSuchElementException;

//Требуется реализовать рест-методы для получения истории /order/history и перевода со счета на счет /order/transfer
//я малость изменил вид запроса, вставив между "order" и названием метода уникальный идентификатор пользователя -
//в данном случае строка, если учесть, что у всех пользователей имена уникальные в рамках данного задания.
//В случае очень большого количества пользователей уникальный идентификатор пользователя можно сменить на тип int или long.

@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class Controller {

    private final Facade facade;

    //как я считаю, требуется получить историю переводов какого-либо клиента, поэтому реализовал
    //получение массива всех переводов

    @GetMapping(value = "{clientName}/history", produces = {"application/json"})
    public ResponseEntity<ArrayList<TransferRecord>> getTransferHistory(@PathVariable("clientName")
                                                                                    String clientName){
        try{
            ArrayList<TransferRecord> clientTransfers = facade.getClientTransfers(clientName);
            return new ResponseEntity<>(clientTransfers, HttpStatus.OK);
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //история одного перевода
    @GetMapping(value = "{clientName}/{transactionId}", produces = {"application/json"})
    public ResponseEntity<TransferRecord> getClientsTransaction(@PathVariable("clientName") String clientName,
                                                                @PathVariable("transactionId") long transId){
        try {
            TransferRecord record = facade.getClientTransferById(clientName, transId);
            return new ResponseEntity<>(record, HttpStatus.OK);
        }catch (NoSuchElementException e){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Данного перевода не существует");
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    //для метода перевода со счета на счет
    //счел необходимым возвращать сгенерированый уникальный id какого-либо нового перевода
    @PostMapping(value = "{clientName}/transfer", produces = {"application/json"})
    public ResponseEntity<Long> pushTransfer(@RequestBody Transfer transfer,
                                             @PathVariable("clientName") String clientName){
        Long transferId;
        try {
            transferId = facade.addTransferToClient(transfer, clientName);
            return new ResponseEntity<>(transferId, HttpStatus.OK);
        } catch (InsufficientFundsException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
