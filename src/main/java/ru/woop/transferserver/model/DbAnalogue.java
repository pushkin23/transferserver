package ru.woop.transferserver.model;

import lombok.Getter;
import ru.woop.transferserver.exceptions.InsufficientFundsException;
import ru.woop.transferserver.model.entities.Client;
import ru.woop.transferserver.model.entities.Transfer;
import ru.woop.transferserver.model.entities.TransferRecord;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

//todo add exceptions!!!
public class DbAnalogue {

    @Getter
    private final ConcurrentHashMap<String, Client>     clientMap;
    private final Set<Long>                             uniqueTransferIds;
    private static Random                               rand = new Random();

    public DbAnalogue() {
        clientMap = new ConcurrentHashMap<>();
        uniqueTransferIds = ConcurrentHashMap.newKeySet();
    }

    public ArrayList<TransferRecord> getClientTransfers(String someClientName) throws IllegalArgumentException{
        checkIfClientExists(someClientName);
        return clientMap.get(someClientName)
                .getTransferRecordHistory();
    }

    private void checkIfClientExists(String clientName) throws IllegalArgumentException {
        if(!clientMap.containsKey(clientName))
            throw new IllegalArgumentException("Такого пользователя не существует!");
    }

    public long addTransferToClient(Transfer transfer, String clientName) throws InsufficientFundsException {
        checkIfClientExists(clientName);
        long uniqueId;
        while(uniqueTransferIds.contains(uniqueId = rand.nextLong()));
        Date initialDate = new Date();
        clientMap.get(clientName)
                .addTransfer(transfer, uniqueId, initialDate);
        return uniqueId;
    }

    public void addClient(String someClientName, Client client){
        clientMap.put(someClientName, client);
    }

    public void removeClient(String someClientName){
        clientMap.remove(someClientName);
    }

    public void deleteDb(){
        clientMap.clear();
    }
}
