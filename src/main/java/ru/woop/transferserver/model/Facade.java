package ru.woop.transferserver.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.woop.transferserver.exceptions.InsufficientFundsException;
import ru.woop.transferserver.model.entities.Transfer;
import ru.woop.transferserver.model.entities.TransferRecord;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class Facade {
    private final DbAnalogue dbAnalogue;

    public ArrayList<TransferRecord> getClientTransfers(String someClientName){
        return dbAnalogue.getClientTransfers(someClientName);
    }

    public TransferRecord getClientTransferById(String clientName, long id){
        return dbAnalogue.getClientTransfers(clientName)
                .stream()
                .filter(a -> a.getId() == id)
                .findFirst()
                .get();
    }

    public long addTransferToClient(Transfer transfer, String accName) throws InsufficientFundsException {
        return dbAnalogue.addTransferToClient(transfer, accName);
    }
}
