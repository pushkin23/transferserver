package ru.woop.transferserver.model.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import ru.woop.transferserver.exceptions.InsufficientFundsException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

@Getter
@EqualsAndHashCode
public class Client {
    private HashMap<String, Double>     accounts;
    private ArrayList<TransferRecord>   transferRecordHistory;

    public Client() {
        accounts = new HashMap<>();
        transferRecordHistory = new ArrayList<>();
    }

    public void addAccount(String accName, double money){
        accounts.put(accName, money);
    }

    //возможно, данный метод в модели - лишнее, но почему бы и нет
    public void addTransfer(Transfer transfer, long uniqueId, Date date) throws InsufficientFundsException {
        String to = transfer.getToAccount();
        String from = transfer.getFromAccount();

        checkIfAccountExist(to);
        checkIfAccountExist(from);

        double toAmount = accounts.get(to);
        double fromAmount = accounts.get(from);
        double amountToTransfer = transfer.getAmount();

        double delta = fromAmount - amountToTransfer ;
        if(delta < 0){
            throw new InsufficientFundsException("Средств для перевода недостаточно!");
        }else{
            accounts.replace(from, delta);
            accounts.replace(to, toAmount + transfer.getAmount());
        }
        transferRecordHistory.add(new TransferRecord(uniqueId, transfer, date));
    }

    private void checkIfAccountExist(String acc) throws IllegalArgumentException{
        if(!accounts.containsKey(acc))
            throw new IllegalArgumentException(String.format("Счета %s не существует!", acc));
    }
}
