package ru.woop.transferserver.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Transfer {
    private String              fromAccount;
    private String              toAccount;
    private double              amount;
}
