package ru.woop.transferserver.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Setter
public class TransferRecord {
    private static SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Getter
    private long            id;

    @JsonUnwrapped
    @Getter
    private Transfer        transfer;

    private Date            date;

    public String getDate() {
        return simpleDateFormat
                .format(date);
    }
}
